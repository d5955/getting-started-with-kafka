from kafka import KafkaProducer
import time

producer = KafkaProducer(bootstrap_servers=['localhost:9092'])
for i in range(1,100):
    msg = 'Sending Number ' + str(i)
    producer.send('tdcdemo', key=b'Message', value=msg.encode())
    producer.flush()
    time.sleep(2)